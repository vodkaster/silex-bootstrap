<?php

namespace Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AdminController
{
  public function indexAction(Application $app) {
    $response = new Response($app['twig']->render('admin/index.html.twig'));
    return $response;
  }
}

