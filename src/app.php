<?php
use Silex\Application;
use Silex\Provider;
use Monolog\Handler\ChromePHPHandler;
use Monolog\Handler\RotatingFileHandler;
use User\UserProvider;

/*
*  Init app and services providers
*/
$app            = new Silex\Application();
$app['debug']   = $config['debug'];
$app['config']  = $config;

$app->register(new Provider\UrlGeneratorServiceProvider());
$app->register(new Provider\SessionServiceProvider());
$app->register(new Provider\ServiceControllerServiceProvider());

// Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  // 'twig.options' => array('cache' => __DIR__.'/../cache', 'strict_variables' => true),
  'twig.options' => array('strict_variables' => true),
  'twig.path' => __DIR__.'/../views',
));

// Gestion des logs
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => __DIR__.'/../log/all.log',
  'monolog.name' => 'boostrap'
));

// Cache
$app->register(new Provider\HttpCacheServiceProvider(), array(
  'http_cache.cache_dir' => __DIR__.'/../cache/'
));

$app->register(new Provider\DoctrineServiceProvider(), array('db.options' => $config['db.options']));

// Login
$app->register(new Silex\Provider\SecurityServiceProvider());
$app['security.firewalls'] = array(
  'login' => array(
    'pattern' => '^/admin/login$',
  ),
  'admin' => array(
    'pattern' => '^/(admin|ajax)',
    'form' => array('login_path' => '/admin/login', 'check_path' => '/admin/login_check'),
    'logout' => array('logout_path' => '/admin/logout'),
    // admin / foo
    'users' => array(
      'admin' => array('ROLE_ADMIN', '5FZ2Z8QIkA7UTZ4BYkoC+GsReLf569mSKDsfods6LYQ8t+a8EW9oaircfMpmaLbPBh4FOBiiFyLfuZmTSUwzZg=='),
    ),
    // 'users' => $app->share(function () use ($app) {
    //   return new UserProvider($app['db']);
    // }),
  ),
);

$app['security.role_hierarchy'] = array(
    'ROLE_ADMIN' => array('ROLE_EDITO'),
);
