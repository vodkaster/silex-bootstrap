<?php

$config['debug'] = false;

// DB connection info
$config['db.options']['user']     = 'PROD_USER';
$config['db.options']['password'] = 'PROD_PWD';

return $config;
